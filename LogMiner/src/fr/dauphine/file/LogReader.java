package fr.dauphine.file;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import fr.dauphine.log.LogRecord;

public class LogReader {
	
	final static Charset ENCODING = StandardCharsets.UTF_8;
	
	public static List<LogRecord> readLog(String filename) {
		
		List<LogRecord> records = new ArrayList<LogRecord>();
		Path path = Paths.get(filename);
		
	    try (Scanner scanner =  new Scanner(path, ENCODING.name())){
	    	while (scanner.hasNextLine()){
	    		String line = scanner.nextLine();
	    		String [] serviceData = line.split(" ");
	    		records.add(new LogRecord(
	    				serviceData[0], Double.parseDouble(serviceData[1]),
	    				Boolean.parseBoolean(serviceData[2]), 
	    				Timestamp.valueOf(serviceData[3] + " " + serviceData[4])));
	    	}      
	    } catch (IOException e) {
			e.printStackTrace();
		}
	    
	    return records;
	}

}
