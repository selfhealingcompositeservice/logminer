package fr.dauphine.logminer;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.dauphine.log.Log;
import fr.dauphine.log.LogRecord;

public class LogMiner {
	Log log;
	Map<String, Integer> ocurrences;
	Map<String, Integer> success;
	List<ServiceStatistics> stats;

	double minExecutionTime = Double.POSITIVE_INFINITY;
	double maxExecutionTime = 0.0;
	Timestamp minTimeStamp = new Timestamp(Long.MAX_VALUE); 
	Timestamp maxTimeStamp = new Timestamp(0);
	
	final static double minNormamization = 0.1;
	final static double maxNormamization = 0.9;
	
	public LogMiner(String filename) {
		log = new Log(filename);
		ocurrences = new HashMap<String, Integer>();
		success = new HashMap<String, Integer>();
		stats = new ArrayList<ServiceStatistics>();
		mine();
	}
	
	private void mine() {
		
		for(LogRecord r:log.getRecords()) {
			
			if(r.getExecutionTime() < minExecutionTime)
				minExecutionTime = r.getExecutionTime();
			else if(r.getExecutionTime() > maxExecutionTime)
				maxExecutionTime = r.getExecutionTime();
			
			if(r.getTimeStamp().compareTo(minTimeStamp) < 0)
				minTimeStamp = r.getTimeStamp();
			else if(r.getTimeStamp().compareTo(maxTimeStamp) > 0)
				maxTimeStamp = r.getTimeStamp();
			
			if(ocurrences.get(r.getServiceName()) == null)
				ocurrences.put(r.getServiceName(), 1);
			else
				ocurrences.put(r.getServiceName(), 
						ocurrences.get(r.getServiceName()) + 1);
			
			if(r.isSuccess()) {
				if(success.get(r.getServiceName()) == null)
					success.put(r.getServiceName(), 1);
				else
					success.put(r.getServiceName(), 
							success.get(r.getServiceName()) + 1);
			}
		}
		
		calculateOldness();
		normalizeExecutionTime();
		System.out.println(log);
		calculateFrequency();
		calculateQoSAverage();
		calculateAvailability();
		System.out.println(stats);
	}
	
	
	/**
     * Calculates the QoS weighted average of each service
     * The QoS is calculated using only successful executions
     * QoSAvg(s) = (sum(QoS(r))*oldness(r)) / sum(oldness(r))
     * @param the record r
     * @return sum of QoS values of r
     */
	private void calculateQoSAverage() {
		
		for(ServiceStatistics ss:stats) {
			for(LogRecord r:log.getRecords())
				if(r.isSuccess() && ss.getName().equals(r.getServiceName())) {
					//set numerator of QoSAvg
					ss.setQosXoldness(ss.getQosXoldness() + getQos(r)*r.getOldness());
					//set denominator of QoSAvg
					ss.setOldnessSum(ss.getOldnessSum() + r.getOldness());
				}
			//set QoS
			ss.setQosAverage(ss.getQosXoldness()/ss.getOldnessSum());
		}
	}
	
	/**
     * Returns the sum of continuous QoS values of a record r
     * continuous QoS values: execution time
     * @param the record r
     * @return sum of QoS values of r
     */
	private double getQos(LogRecord r) {
		return r.getExecutionTimeNormalized();
	}

	
	/**
     * Sets the oldness of each record
     * The higher ts(r), the smaller value 
     * (higher ts(r) -> the older is the record /\ smaller oldness(r))
     * oldness(r) = (ts(r) - maxTs) / (minTs - maxTs)
     */
	private void calculateOldness() {

		for(LogRecord r:log.getRecords())
			r.setOldness(scale(
					normalize(r.getTimeStamp().getTime(), 
							minTimeStamp.getTime(), maxTimeStamp.getTime())
					));
	}
	
	
	/**
     * Normalizes the execution time of each record
     * The higher et(r), the smaller value (the higher, the worse)
     *  (higher et(r) -> smaller etN(r))
     * etN(r) = (et(r) - maxExecutionTime) / (minExecutionTime - maxExecutionTime)
     */
	private void normalizeExecutionTime() {
		for(LogRecord r:log.getRecords())
			r.setExecutionTimeNormalized(scale(
					normalize(r.getExecutionTime(), minExecutionTime, maxExecutionTime)
					));
	}
	
	
	/**
     * Sets the relative frequency of execution of each service
     * The higher ocurrence(s), the higher value
     * f(s) = (ocurrence(s) - minOcurrence) / (maxOcurrence - minOcurrence)
     */
	private void calculateFrequency() {
		int maxOcurrence = 0;
		int minOcurrence = (int) Double.POSITIVE_INFINITY;
		
		//get max and min ocurrences
		for(Integer i:ocurrences.values())
			if(i > maxOcurrence)
				maxOcurrence = i;
			else if(i < minOcurrence)
				minOcurrence = i;
		
		//set the frequency of each service
		for(String s:ocurrences.keySet()) {
			
			ServiceStatistics ss = new ServiceStatistics(s);
			ss.setFrequency(scale(
					normalize(ocurrences.get(s), maxOcurrence, minOcurrence)
					));
			
			stats.add(ss);
		}
	}
	
	private void calculateAvailability() {
		for(ServiceStatistics ss:stats)
			ss.setAvailability(success.get(ss.getName())/ocurrences.get(ss.getName()));
	}
	
	/**
     * Returns a normalized value between a range [min,max]
     * (value - min) / (max - min)
     * @param the value to be scaled
     * @param the max value for the normalization
     * @param the min value for the normalization
     * @return a scaled value between a range [min,max]
     */
	private double normalize(double d, double max, double min) {
		return (d - min)/(max - min);
	}
	
	
	/**
     * Returns a scaled value between a range [min,max]
     * (value*(max-min) + min
     * @param the value to be scaled
     * @return a scaled value between a range [min,max]
     */
	private double scale(double d) {
		return (d*(maxNormamization-minNormamization)) + minNormamization;
	}

}
