package fr.dauphine.logminer;

public class ServiceStatistics {
	
	private String name;
	private double frequency;
	private double qosXoldness;
	private double qosLog;
	private double oldnessSum;
	private double qosAverage;
	private double availability;

	public ServiceStatistics(String name) {
		super();
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	public double getFrequency() {
		return frequency;
	}

	public void setFrequency(double frequency) {
		this.frequency = frequency;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServiceStatistics other = (ServiceStatistics) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ServiceStatistics [name=" + name + ", frequency=" + frequency
				+ ", qosXoldness=" + qosXoldness + ", qosLog=" + qosLog
				+ ", oldnessSum=" + oldnessSum + ", qosAverage=" + qosAverage
				+ ", availability=" + availability + "]";
	}

	public double getQosLog() {
		return qosLog;
	}

	public void setQosLog(double qosLog) {
		this.qosLog = qosLog;
	}

	public double getQosXoldness() {
		return qosXoldness;
	}

	public void setQosXoldness(double qosXoldess) {
		this.qosXoldness = qosXoldess;
	}

	public double getOldnessSum() {
		return oldnessSum;
	}

	public void setOldnessSum(double oldnessSum) {
		this.oldnessSum = oldnessSum;
	}

	public double getQosAverage() {
		return qosAverage;
	}

	public void setQosAverage(double qosAverage) {
		this.qosAverage = qosAverage;
	}

	public double getAvailability() {
		return availability;
	}

	public void setAvailability(double availability) {
		this.availability = availability;
	}

}
