package fr.dauphine.log;

import java.util.List;

import fr.dauphine.file.LogReader;

public class Log {
	
	private List<LogRecord> records;
	
	public Log(String filename) {
		records = LogReader.readLog(filename);
		System.out.println(records);
	}

	public List<LogRecord> getRecords() {
		return records;
	}

	@Override
	public String toString() {
		return "Log [records=" + records + "]";
	}

}
